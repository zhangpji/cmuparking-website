import { combineReducers } from 'redux'
import { RootAction } from './actions'
import * as actions from './actions'
import { RootState,
         UserState, defaultUserState,
         LocationsState, defaultLocationsState,
         ReservationsState, defaultReservationsState,
         MessagesState, defaultMessagesState,
       } from './states'
import { dissoc, toMap } from '../utils/shoyu'

// User
const user = (state = defaultUserState, action: RootAction): UserState => {
  switch (action.type) {
    case actions.ADD_USER:
      return action.payload

    case actions.DEL_USER:
      return defaultUserState

    default:
      return state
  }
}

// Locations
const locations = (state = defaultLocationsState, action: RootAction): LocationsState => {
  switch (action.type) {
    case actions.WAIT_LOCATIONS:
      return { ...state,
               pending: true }

    case actions.ADD_LOCATIONS:
      return { ...state,
               pending: false,
               data: toMap(action.payload) }

    default:
      return state
  }
}

// Reservations
const reservations = (state = defaultReservationsState, action: RootAction): ReservationsState => {
  switch (action.type) {
    case actions.WAIT_RESERVATIONS:
      return { ...state,
               pending: true }

    case actions.ADD_RESERVATIONS:
      return { ...state,
               pending: false,
               isLast: action.payload.isLast,
               data: toMap(action.payload.reservations)}

    default:
      return state
  }
}

// Messages
const messages = (state = defaultMessagesState, action: RootAction): MessagesState => {
  switch (action.type) {
    case actions.ADD_MESSAGE:
      return { ...state,
               [action.payload.id]: action.payload.message }

    case actions.DEL_MESSAGE:
      return dissoc(state, action.payload)

    case actions.CLEAR_MESSAGES:
      return defaultMessagesState

    default:
      return state
  }
}

export const rootReducer = combineReducers<RootState>({
  user,
  locations,
  reservations,
  messages,
})
