import { ThunkAction } from 'redux-thunk'
import { v4 as uuid } from 'uuid'
import { urls } from '../constants'
import { delay } from '../utils/shoyu'
import request, { revokeTokens } from '../utils/auth'
import { Result, update } from '../utils/request'
import { Reservation, User, Message, MessageType, RootState } from './states'

type ReservationsRes = {
  isLast: boolean,
  reservations: Reservation[]
}

// Sync actions
// Action names
export const ADD_USER = 'ADD_USER'
export const DEL_USER = 'DEL_USER'

export const WAIT_LOCATIONS = 'WAIT_LOCATIONS'
export const ADD_LOCATIONS = 'ADD_LOCATIONS'

export const WAIT_RESERVATIONS = 'WAIT_RESERVATIONS'
export const ADD_RESERVATIONS = 'ADD_RESERVATIONS'

export const ADD_MESSAGE = 'ADD_MESSAGE'
export const DEL_MESSAGE = 'DEL_MESSAGE'
export const CLEAR_MESSAGES = 'CLEAR_MESSAGES'

// Action types
type Actions = {
  ADD_USER: {
    type: typeof ADD_USER,
    payload: User,
  },
  DEL_USER: {
    type: typeof DEL_USER,
  },

  WAIT_LOCATIONS: {
    type: typeof WAIT_LOCATIONS,
  },
  ADD_LOCATIONS: {
    type: typeof ADD_LOCATIONS,
    payload: Location[],
  },

  WAIT_RESERVATIONS: {
    type: typeof WAIT_RESERVATIONS,
  },
  ADD_RESERVATIONS: {
    type: typeof ADD_RESERVATIONS,
    payload: ReservationsRes,
  }

  ADD_MESSAGE: {
    type: typeof ADD_MESSAGE,
    payload: {
      id: string,
      message: Message,
    }
  },
  DEL_MESSAGE: {
    type: typeof DEL_MESSAGE,
    payload: string,
  },
  CLEAR_MESSAGES: {
    type: typeof CLEAR_MESSAGES,
  },
}

// Actions creators
export type RootAction = Actions[keyof Actions]

export const actions = {
  addUser: (user: User): Actions[typeof ADD_USER] => ({
    type: ADD_USER,
    payload: user,
  }),
  delUser: (): Actions[typeof DEL_USER] => ({
    type: DEL_USER,
  }),

  waitLocations: (): Actions[typeof WAIT_LOCATIONS] => ({
    type: WAIT_LOCATIONS,
  }),
  addLocations: (locations: Location[]): Actions[typeof ADD_LOCATIONS] => ({
    type: ADD_LOCATIONS,
    payload: locations,
  }),

  waitReservations: (): Actions[typeof WAIT_RESERVATIONS] => ({
    type: WAIT_RESERVATIONS,
  }),
  addReservations: (reservationsRes: ReservationsRes): Actions[typeof ADD_RESERVATIONS] => ({
    type: ADD_RESERVATIONS,
    payload: reservationsRes,
  }),

  addMessage: (id: string, message: string, type: MessageType): Actions[typeof ADD_MESSAGE] => ({
    type: ADD_MESSAGE,
    payload: { id,
               message: { type,
                          message }},
  }),
  delMessage: (id: string): Actions[typeof DEL_MESSAGE] => ({
    type: DEL_MESSAGE,
    payload: id
  }),
  clearMessages: (): Actions[typeof CLEAR_MESSAGES] => ({
    type: CLEAR_MESSAGES,
  }),
}

// Async actions

// Helpers
const fetchUser = async (): Promise<Result<User>> => {
  const res = await request.get(`${urls.api}/me`, true)
  return update(res, (x) => ({
    id: x.id,
    email: x.email,
    name: x.name,
    plateNumber: x['plate_number'],
    phoneNumber: x['phone_number'],
    avatarURL: x['avatar_url'],
  }))
}

const fetchLocations = async (): Promise<Result<Location[]>> => {
  const res = await request.get(`${urls.api}/locations`)
  return update(res, (x) => (x.map((y) => ({
    id: y.id,
    name: y.name,
    lat: y.lat,
    lng: y.lng,
  }))))
}

const fetchReservations = async (): Promise<Result<ReservationsRes>> => {
  const res = await request.get(`${urls.api}/me/reservations`, true,
                                { states: ['WAITING',
                                           'STARTED',
                                           'PENDING'] })
  return update(res, x => ({
    reservations: x['reservations'].map(y => ({
      id: y.id,
      locationId: y['location_id'],
      spotName: y['spot_name'],
      state: y.state,
      start: new Date(y.start),
      end: new Date(y.end)
    })),
    isLast: x['is_last']
 }))
}

const addMessageWithTimeout = (type: MessageType) => (message: string): AsyncAction<void, {}> => {
  return async (dispatch) => {
    const id = uuid()
    dispatch(actions.addMessage(id, message, type))
    await delay(5000)
    dispatch(actions.delMessage(id))
  }
}

type AsyncAction<R, E> = (ThunkAction<R, RootState, E>)

// Actions creators
export const asyncActions = {
  loadUser: () => async (dispatch) => {
    const user = await fetchUser()
    switch (user.type) {
      case 'SUCCESS':
        dispatch(actions.addUser(user.data))
        break

      case 'ERROR':
        for (let msg of user.messages) {
          dispatch(asyncActions.addError(msg))
        }
    }
  },

  logout: () => async (dispatch) => {
    await revokeTokens()
    dispatch(actions.delUser())
  },

  loadLocations: () => async (dispatch) => {
    const locations = await fetchLocations()
    switch (locations.type) {
      case 'SUCCESS':
        dispatch(actions.addLocations(locations.data))
        break

      case 'ERROR':
        for (let msg of locations.messages) {
          dispatch(asyncActions.addError(msg))
        }
    }
  },

  loadReservations: () => async (dispatch) => {
    const reservations = await fetchReservations()
    switch (reservations.type) {
      case 'SUCCESS':
        dispatch(actions.addReservations(reservations.data))
        break

      case 'ERROR':
        for (let msg of reservations.messages) {
          dispatch(asyncActions.addError(msg))
        }
    }
  },

  addSuccess: addMessageWithTimeout('SUCCESS'),
  addError: addMessageWithTimeout('ERROR'),
}
