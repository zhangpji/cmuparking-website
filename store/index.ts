import { createStore, applyMiddleware, Store } from 'redux'
import * as nextRedux from 'next-redux-wrapper'
import thunkMiddleware from 'redux-thunk'
import { RootState } from './states'
import { rootReducer } from './reducers'

export const makeStore = (state: RootState): Store<RootState> => {
  return createStore(
    rootReducer,
    state,
    applyMiddleware(thunkMiddleware)
  )
}

export default (mapStateToProps?, mapDispatchToProps?) => (
  nextRedux(makeStore, mapStateToProps, mapDispatchToProps)
)
