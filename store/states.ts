// User
export type User = {
  readonly id: string
  readonly email: string
  readonly name: string
  readonly plateNumber: string
  readonly phoneNumber: string
  readonly avatarURL: string
}

export type UserState = User

export const defaultUserState: UserState = {
  id: '',
  email: '',
  name: '',
  plateNumber: '',
  phoneNumber: '',
  avatarURL: '',
}

// Locations
export type Location = {
  readonly id: string,
  readonly name: string,
  readonly lat: number,
  readonly lng: number,
}

export type LocationsState = {
  readonly pending: boolean,
  data: {
    readonly [id: string]: Location
  },
}

export const defaultLocationsState: LocationsState = {
  pending: false,
  data: {},
}

// Reservations
export type ReservationState = ('UNPAID'   |
                                'PENDING'  |
                                'WAITING'  |
                                'STARTED'  |
                                'EXTENDING'|
                                'ENDED'    |
                                'CANCELLED'
                               )

export type Reservation = {
  readonly id: string,
  readonly state: ReservationState,
  readonly locationId: string,
  readonly spotName: string,
  readonly start: Date,
  readonly end: Date,
}

export type ReservationsState = {
  readonly pending: boolean,
  readonly isLast: boolean,
  data: {
    readonly [id: string]: Reservation
  }
}

export const defaultReservationsState: ReservationsState = {
  pending: false,
  isLast: false,
  data: {},
}

// Messages
export type MessageType = 'SUCCESS' | 'ERROR'
export type Message = {
  readonly type: MessageType
  readonly message: string
}

export type MessagesState = {
  readonly [uuid: string]: Message
}

export const defaultMessagesState: MessagesState = {}

export type RootState = {
  user: UserState,
  reservations: ReservationsState,
  messages: MessagesState,
}
