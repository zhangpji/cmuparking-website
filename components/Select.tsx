import * as React from 'react'
import { Fragment } from 'react'
import { IoChevronUp, IoChevronDown } from 'react-icons/lib/io'

export type Option = {
  value: string
  label: string
}

interface SelectProps {
  options: Array<Option>
  onSelect: (option: Option) => void
  selectedOption?: Option
  disabled?: boolean
}

type SelectState = {
  isOpen: boolean
}

export class Select extends React.Component<SelectProps, SelectState> {
  constructor (props) {
    super(props)
    this.state = {
      isOpen: false
    }
  }

  render () {
    const { isOpen } = this.state
    const { options, selectedOption, disabled } = this.props
    return (
      <Fragment>
        <div className={`select${disabled ? ' disabled' : ''}`}>
          <div className="select__display"
               onClick={() => {this.setState(prevState => ({ isOpen: !prevState.isOpen }))}}>
            <span className="select__selected-option">
              {(selectedOption && selectedOption.label)
               || '-- SELECT --'}
            </span>
            <span className="select__arrow">
              {isOpen ? <IoChevronUp /> : <IoChevronDown />}
            </span>
          </div>
          {isOpen &&
           <div className="select__options">
             <ul className="list--plain">
               {options.map(option => (
                 <li className={`select__option${selectedOption && option.value == selectedOption.value ? ' select__option--selected' : ''}`}
                     onClick={() => {
                         this.props.onSelect(option)
                         this.setState({ isOpen: false })
                     }}
                     key={option.value}>
                   {option.label}
                 </li>
               ))}
             </ul>
           </div>}
        </div>
      </Fragment>
    )
  }
}
