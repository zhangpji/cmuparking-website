import * as React from 'react'
import { Fragment } from 'react'
import css from 'styled-jsx/css'
import { IoPause, IoCheckmark, IoLocked,
         IoClock, IoLocation, IoTrashA, IoCompose } from 'react-icons/lib/io'
import { ReservationsState,
         Reservation,
         Location,
         LocationsState } from '../store/states'
import { urls } from '../constants'
import { isEmptyObj, compareField, formatTime } from '../utils/shoyu'
import request from '../utils/auth'
import { BouncingLoader } from './Loader'

const groupReservationByDate = (reservations: Reservation[], sort: boolean = true): { [date: string]: Reservation[] } => {
  const reduceFn = (xs: { [date: string]: Reservation[] }, y: Reservation) => {
    const yDate = formatTime(y.start, 'MM/DD YYYY')
    const ys = xs[yDate] || []
    return {
      ...xs,
      [yDate]: [...ys, y]
    }
  }

  const unsortedRes = reservations.reduce(reduceFn, {})

  if (sort) {
    return Object.keys(unsortedRes)
                 .sort()
                 .reduce((xs, k) => ({
                   ...xs,
                   [k]: unsortedRes[k]
                 }), {})
  } else {
    return unsortedRes
  }
}

const reservationCompareFn = (x: Reservation, y: Reservation): number => {
  return compareField(x, y, 'start',
                      (x, y) => compareField(x, y, 'end'))
}

const stateToDescription = {
  'PENDING': [<IoPause />, 'Waiting for spot allocation.'],
  'WAITING': [<IoCheckmark />, 'Waiting for you to check in.'],
  'STARTED': [<IoCheckmark />, 'You have checked in.'],
  'ENDED': [<IoLocked />, 'Your reservation has ended.'],
}

const buttonStyles = css`
.icon {
  opacity: 0.7;
  margin-right: 0.5em;
}

button {
  width: 100%;
  margin: 0.5em auto;
}`


const getButtons = (
  reservation: Reservation,
  actions: {
    checkin: () => Promise<void>,
    checkout: () => Promise<void>,
    cancel: () => Promise<void>,
  }): JSX.Element => {
    switch (reservation.state) {
      case 'WAITING':
        return (
          <Fragment>
            <button className="center" onClick={actions.checkin}>
              <span className="icon"><IoCompose /></span>
              <span>Check in</span>
            </button>
            <button className="center" onClick={actions.cancel}>
              <span className="icon"><IoTrashA /></span>
              <span>Cancel</span>
            </button>
            <style jsx>{buttonStyles}</style>
          </Fragment>
        )
      case 'STARTED':
        return (
          <Fragment>
            <button className="center" onClick={actions.checkout}>
              <span className="icon"><IoCompose /></span>
              <span>Check out</span>
            </button>
            <style jsx>{buttonStyles}</style>
          </Fragment>
        )

      default:
        return (
          <Fragment>
            <button className="center" onClick={actions.cancel}>
              <span className="icon"><IoTrashA /></span>
              <span>Cancel</span>
            </button>
            <style jsx>{buttonStyles}</style>
          </Fragment>
        )
    }
  }

interface ReservationGroupProps {
  date: string
  reservations: Reservation[]
  locations: LocationsState
  actions: {
    checkin: (id: string) => Promise<void>,
    checkout: (id: string) => Promise<void>,
    cancel: (id: string) => Promise<void>,
  }
}

const ReservationGroup: React.SFC<ReservationGroupProps> = ({ date, reservations, locations, actions }) => (
  <Fragment>
    <div className="row reservation__group">
      <div className="reservation__date three cols">{date}</div>
      <div className="reservation__items nine cols">
        {reservations.sort(reservationCompareFn).map(x => (
          <ReservertionItem
            reservation={x}
            location={locations.data[x.locationId]}
            key={x.id}
            actions={{
              checkin: () => actions.checkin(x.id),
              checkout: () => actions.checkout(x.id),
              cancel: () => actions.cancel(x.id),
            }}
          />
        ))}
      </div>
    </div>
    <style jsx>{`
.reservation__group {
  border-top: 2px solid rgba(0, 0, 0, 0.11);
  padding: 1em 0;
  align-items: flex-start;
}

.reservation__date {
  font-size: 1.4em;
  font-weight: 300;
}

@media(max-width: 720px) {
  .reservation__group {
    padding-top: 0;
  }
  .reservation__date {
    margin-bottom: 20px;
    background-color: rgba(0, 0, 0, 0.03);
    padding: 10px 8px;
  }
}
      `}
    </style>
  </Fragment>
)

interface ReservationItemProps {
  reservation: Reservation
  location: Location
  actions: {
    checkin: () => Promise<void>,
    checkout: () => Promise<void>,
    cancel: () => Promise<void>,
  }
}

const ReservertionItem: React.SFC<ReservationItemProps> = (props) => {
  const { reservation, location, actions } = props
  const [icon, description] = stateToDescription[reservation.state]
  return (
    <Fragment>
      <div className="row reservation__item">
        <div className="nine cols">
          <div className="reservation__item__state">
            <span className="icon">
              {icon}
            </span>
            <span className="reservation__item__state__description">
              {description}
            </span>
          </div>
          <div className="reservation__item__time">
            <span className="icon">
              <IoClock />
            </span>
            <span className="reservation__item__start">
              {formatTime(reservation.start, 'HH:mm')}
            </span>
            <span className="separator">to</span>
            <span className="reservation__item__end">
              {formatTime(reservation.end, 'HH:mm')}
            </span>
          </div>
          <div className="reservation__item__location">
            <span className="icon">
              <IoLocation />
            </span>
            {reservation.spotName &&
             <Fragment>
               <span className="reservation__item__spot">
                 Spot {reservation.spotName}
               </span>
               <span className="separator">@</span>
             </Fragment>}
            <span>
              {location.name}
            </span>
          </div>
        </div>
        <div className="three cols">
          {getButtons(reservation, actions)}
        </div>
      </div>
      <style jsx>{`
.icon {
  margin-right: 10px;
  opacity: 0.7;
  font-size: 24px;
}

.separator {
  margin: 0 0.7em;
}

.reservation__item {
  padding-bottom: 1em;
}

.reservation__item:not(:last-child) {
  margin-bottom: 1em;
  border-bottom: 1px solid rgba(0, 0, 0, 0.11);
}

.reservation__item__time {
  margin-bottom: 0.7em;
  display: flex;
  align-items: center;
}

.reservation__item__start,
.reservation__item__end {
  font-weight: 700;
}

.reservation__item__time__separator {
  margin: 0 1em;
}

.reservation__item__state {
  display: flex;
  align-items: center;
  font-size: 1.17em;
  margin-bottom: 0.7em;
  font-style: italic;
}

.reservation__item__location {
  display: flex;
  align-items: center;
}
.reservation__item__spot {
  font-weight: 700;
}
        `}
      </style>
    </Fragment>
  )
}

interface ReservationListProps {
  reservations: ReservationsState
  loadReservations: () => void
  locations: LocationsState
  loadLocations: () => void
}

type ReservationListState = {

}

const initialState = {}

export class ReservationList extends React.Component<ReservationListProps, ReservationListState> {
  constructor (props) {
    super(props)
    this.state = initialState
  }

  componentDidMount () {
    if (isEmptyObj(this.props.locations.data)) {
      this.props.loadLocations()
    }

    if (isEmptyObj(this.props.reservations.data)) {
      this.props.loadReservations()
    }
  }

  render () {
    const { reservations, locations } = this.props
    const isPending = reservations.pending && locations.pending
    const groupedReservations = Object.entries(
      groupReservationByDate(Object.values(reservations.data))
    )
    return (
      <Fragment>
        {isPending
         ? <BouncingLoader />
         : (
           <section className="reservation-list">
             {groupedReservations.map(x => (
               <ReservationGroup
                 date={x[0]}
                 reservations={x[1]}
                 locations={locations}
                 actions={
                   { checkin: this.checkin,
                     checkout: this.checkout,
                     cancel: this.cancel }
                 }
                 key={x[0]}
               />
             ))}
             {reservations.isLast
              || (
                <div className="reservation-list__footer right">
                  <button>Load more</button>
                </div>
              )}
           </section>
         )}
        <style jsx>{`
.reservation-list {
  margin: 2em auto;
}

.reservation-list__footer {
  border-top: 2px solid rgba(0, 0, 0, 0.11);
  padding: 1em 0;
}
          `}
        </style>
      </Fragment>
    )
  }

  checkin = async (id: string): Promise<void> => {
    const res = await request.patch(`${urls.api}/me/reservations/${id}`,
                                    true,
                                    { mode: 'CHECKIN' })
    switch (res.type) {
      case 'SUCCESS':
        this.props.loadReservations()
    }
  }

  checkout = async (id: string): Promise<void> => {
    if (window.confirm('This action cannot be undone. Continue?')) {
      const res = await request.patch(`${urls.api}/me/reservations/${id}`,
                                      true,
                                      { mode: 'CHECKOUT' })
      switch (res.type) {
        case 'SUCCESS':
          this.props.loadReservations()
      }
    }
  }

  cancel = async (id: string): Promise<void> => {
    if (window.confirm('This action cannot be undone. Continue?')) {
      const res = await request.delete(`${urls.api}/me/reservations/${id}`,
                                       true)
      switch (res.type) {
        case 'SUCCESS':
          this.props.loadReservations()
      }
    }
  }
}
