import * as React from 'react'
import { Fragment } from 'react'

interface ActionBarProps {
  title: string
}

export const ActionBar: React.SFC<ActionBarProps> = (props) => (
  <Fragment>
    <div className="bar">
      <h2>{props.title}</h2>
      <section className="right actions">
        {props.children}
      </section>
    </div>
    <style jsx>{`
h2 {
  font-size: 1.7rem;
  margin: 0;
}

@media(max-width: 720px) {
  .bar {
    display: block;
    text-align: center;
  }

  .actions {
    margin-top: 20px;
    display: flex;
    flex-wrap: nowrap;
    justify-content: center;
  }
}
      `}
    </style>
  </Fragment>
)
