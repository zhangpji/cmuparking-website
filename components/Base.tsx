import * as React from 'react'
import { Fragment } from 'react'
import Head from 'next/head'
import { initGA, logPageview } from '../utils/ga'

import 'normalize.css'
import './base.css'

interface BaseProps {
  title: string
}

type BaseState = {}

export class Base extends React.Component<BaseProps, BaseState> {
  constructor (props) {
    super(props)
    this.state = {}
  }

  componentDidMount () {
    initGA()
    logPageview()
  }

  render () {
    return (
      <Fragment>
        <Head>
          <meta name="viewport" content="initial-scale=1.0, width=device-width" />
          <meta charSet="utf-8" />
          <title>{this.props.title}</title>
          <link href="/_next/static/style.css" rel="stylesheet" />
        </Head>
        <div className="page">
          {this.props.children}
        </div>
      </Fragment>
    )
  }
}
