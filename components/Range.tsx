import * as React from 'react'
import { Fragment } from 'react'

interface RangeProps {
  value: number
  onChange: (value: number) => void
  range: [number, number]
  step?: number
}

export const Range: React.SFC<RangeProps> = (props) => {
  const [min, max] = props.range
  return (
    <Fragment>
      <div className="range-input">
        <span className="range-input__range">{min}</span>
        <input type="range" value={props.value}
               step={props.step} min={min} max={max}
               onChange={e => props.onChange(+e.target.value)} />
        <span className="range-input__range">{max}</span>
      </div>
    </Fragment>
  )
}
