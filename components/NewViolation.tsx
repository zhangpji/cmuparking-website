import * as React from 'react'
import { Fragment } from 'react'
import { LocationsState } from '../store/states'
import { isEmptyObj, toMap } from '../utils/shoyu'
import { update } from '../utils/request'
import request from '../utils/auth'
import { urls } from '../constants'
import { Select, Option } from './Select'
import { BouncingLoader } from './Loader'
import { Mask } from './Mask'

const locationsToOptions = (locations: LocationsState): Option[] => (
  Object.values(locations.data).map(x => ({
    value: x.id,
    label: x.name,
  }))
)

type Spots = {
  [id: string]: {
    id: string,
    name: string,
  }
}

const spotsToOptions = (spots: Spots): Option[] => (
  Object.values(spots).map(x => ({
    value: x.id,
    label: x.name,
  }))
)

interface NewViolationProps {
  locations: LocationsState
  loadLocations: () => void
  onSuccess: () => void
}

type NewViolationState = {
  locationId: string,
  spotId: string,
  spots: Spots,
  comment: string,
  photo: File | null
}

export class NewViolation extends React.Component<NewViolationProps, NewViolationState> {
  constructor(props) {
    super(props)
    const locations = Object.values(this.props.locations.data)
    const selectedLocationId = locations.length > 0 ? locations[0].id : ''
    this.state = {
      locationId: selectedLocationId,
      spotId: '',
      comment: '',
      spots: {},
      photo: null,
    }
  }

  async componentDidMount () {
    if (isEmptyObj(this.props.locations.data)) {
      this.props.loadLocations()
    }
    await this.fetchSpots()
  }

  render () {
    const { locations } = this.props
    const locationOptions = locationsToOptions(locations)
    const selectedLocation = locations.data[this.state.locationId]

    const { spots } = this.state
    const spotOptions = spotsToOptions(spots)
    const selectedSpot = spots[this.state.spotId]

    const isInputValid = this.state.locationId && this.state.spotId && this.state.photo

    return (
      <Fragment>
        {locations.pending
         ? (
           <div className="center">
             <BouncingLoader />
           </div>
         )
         : (
           <form onSubmit={this.handleSubmit}>
             <section>
               <div className="row row--form">
                 <div className="eight cols">
                   <span className="label">
                     Where did you find the violation?
                   </span>
                   <Select
                     options={locationOptions}
                     selectedOption={selectedLocation && { value: selectedLocation.id,
                                                           label: selectedLocation.name }}
                     onSelect={this.handleLocationSelect}
                   />
                 </div>
                 <div className="four cols">
                   <span className="label">Which spot?</span>
                   <Select
                     options={spotOptions}
                     selectedOption={selectedSpot && { value: selectedSpot.id,
                                                       label: selectedSpot.name }}
                     onSelect={x => { this.setState({ spotId: x.value }) }}
                   />
                 </div>
               </div>
             </section>
             <section>
               <div className="row row--form">
                 <div className="twelve cols">
                   <label htmlFor="photo">
                     Upload a photo:
                   </label>
                   <input name="photo" type="file"
                          onChange={e => { this.setState({ photo: e.target.files[0] })}} />
                 </div>
               </div>
             </section>
             <section>
               <div className="row row--form">
                 <div className="twelve cols">
                   <label htmlFor="comment">Additional information and comments:</label>
                   <textarea name="comment" id="comment"
                             value={this.state.comment}
                             onChange={e => { this.setState({ comment: e.target.value })}}></textarea>
                 </div>
               </div>
             </section>

             <div className="divider"></div>

             <section>
               <div className="row row--form">
                 <Mask enabled={!isInputValid}>
                   <button className="button--primary">Report</button>
                 </Mask>
               </div>
             </section>
           </form>
         )}
      </Fragment>
    )
  }

  handleLocationSelect = async (option: Option) => {
    const locationId = option.value
    this.setState({
      locationId,
      spotId: ''
    })

    await this.fetchSpots()
  }

  handleSubmit = async (e) => {
    e.preventDefault()

    const { spotId, photo, comment } = this.state

    let photoData = new FormData()
    photoData.append('file', photo)
    const uploadRes = await request.post(`${urls.base}/uploads`,
                                         true,
                                         null,
                                         photoData)
    switch (uploadRes.type) {
      case 'SUCCESS':
        const photoId = uploadRes.data['filename']
        const reportRes = await request.post(
          `${urls.api}/violations`,
          true,
          null,
          {
            'spot_id': spotId,
            'photo_id': photoId,
            'comment': comment,
          })
        switch (reportRes.type) {
          case 'SUCCESS':
            this.props.onSuccess()
            break

          case 'ERROR':
            break
        }
        break

      case 'ERROR':
        break
    }

    this.setState({
      spotId: '',
      photo: null,
      comment: '',
    })
  }

  fetchSpots = async () => {
    if (this.state.locationId) {
      const res = await request.get(`${urls.api}/locations/${this.state.locationId}/spots`)
      const spots = update(res, x => x.map(y => ({
        id: y['id'],
        name: y['name']
      })))

      switch (spots.type) {
        case 'SUCCESS':
          this.setState({
            spots: toMap(spots.data)
          })
      }
    }
  }
}
