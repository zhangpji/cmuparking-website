import * as React from 'react'
import { Fragment } from 'react'
import Link from 'next/link'
import { IoEmail, IoIphone, IoModelS,
         IoChevronDown, IoChevronUp,
         IoPower } from 'react-icons/lib/io'
import { User } from '../store/states'

interface NavProps {
  user: User
  logout: () => void
}

export const Nav: React.SFC<NavProps> = (props) => (
  <Fragment>
    <nav>
      <div className="inner bar">
        <div className="brand">
          <Link prefetch href="/">
            <a>
              <img alt="CMU Parking BRAND" src="/static/logo.png" />
              CMU Parking
            </a>
          </Link>
        </div>
        {props.user.email
         ? <AccountMenu {...props} />
         : <div className="links">
           <Link prefetch href="/signin"><a>Sign in</a></Link>
           <Link prefetch href="/signup"><a>Sign up</a></Link>
         </div>}
      </div>
    </nav>
    <style jsx>{`
.brand {
  font-weight: 900;
}

a {
  text-decoration: none;
}

a + a {
  margin-left: 1em;
}

img {
  width: 36px;
  height: 36px;
}
      `}
    </style>
  </Fragment>
)

interface AccountMenuProps {
  user: User
  logout: () => void
}

type AccountMenuState = {
  isOpen: boolean
}

class AccountMenu extends React.Component<AccountMenuProps, AccountMenuState> {
  constructor (props) {
    super(props)
    this.state = {
      isOpen: false
    }
  }

  render () {
    const { user } = this.props
    return (
      <div className="account">
        <span className="link center"
              onClick={() => { this.setState(prevState => ({ isOpen: !prevState.isOpen })) }}>
          {user.name}
          <span className="icon">
            {this.state.isOpen
             ? <IoChevronUp />
             : <IoChevronDown />}
          </span>
        </span>
        {this.state.isOpen &&
         <ProfileCard {...this.props} />}
        <style jsx>{`
.account {
  position: relative;
}

.icon {
  margin-left: 0.5em;
  font-size: 0.7em;
}
          `}
        </style>
      </div>
    )
  }
}

interface ProfileCardProps {
  user: User
  logout: () => void
}

const ProfileCard: React.SFC<ProfileCardProps> = (props) => (
  <div className="card">
    <div className="entry bar">
      <span className="icon"><IoEmail /></span>
      {props.user.email}
    </div>
    {props.user.phoneNumber &&
     <div className="entry bar">
       <span className="icon"><IoIphone /></span>
       {props.user.phoneNumber}
     </div>}
    <div className="entry bar">
      <span className="icon"><IoModelS /></span>
      {props.user.plateNumber}
    </div>
    <div className="right">
      <button className="center" onClick={props.logout}>
        <span className="icon"><IoPower /></span>
        Logout
      </button>
    </div>
    <style jsx>{`
.icon {
  margin-right: 0.7em;
}

.card {
  position: absolute;
  top: 100%;
  right: 0;
  left: -100%;
  background-color: #fffffe;
  box-shadow: 1px 1px 7px rgba(0, 0, 0, 0.1);
  border: 1px solid #dedfdf;
}

.entry {
  padding: 7px 12px;
  border-bottom: 1px solid #dedfdf;
}

button {
  margin: 7px 12px;
  padding: 5px 7px;
}

button .icon {
  font-size: 1em;
  margin-right: 0.3em;
}
      `}</style>
  </div>
)
