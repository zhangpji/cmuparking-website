import * as React from 'react'
import { Fragment } from 'react'
import { LocationsState } from '../store/states'
import { urls } from '../constants'
import { isEmptyObj, generateTimeStrs, formatTime, parseTimeStr } from '../utils/shoyu'
import request from '../utils/auth'
import { Select, Option } from './Select'
import { Range } from './Range'
import { BouncingLoader } from './Loader'
import { Mask } from './Mask'

const locationsToOptions = (locations: LocationsState): Option[] => (
  Object.values(locations.data).map(x => ({
    value: x.id,
    label: x.name,
  }))
)

interface NewReservationProps {
  locations: LocationsState
  loadLocations: () => void
  onSuccess: () => void
}

type NewReservationState = {
  locationId: string
  startDateStr: string
  startTimeStr: string
  duration: number
  maxDuration: number
}

export class NewReservation extends React.Component<NewReservationProps, NewReservationState> {
  todayStr: string

  constructor (props) {
    super(props)
    const locations = Object.values(this.props.locations.data)
    this.state = {
      locationId: locations.length > 0 ? locations[0].id : '',
      startDateStr: '',
      startTimeStr: '',
      duration: 0,
      maxDuration: 0,
    }
    this.todayStr = formatTime(new Date(), 'YYYY-MM-DD')
  }

  componentDidMount () {
    if (isEmptyObj(this.props.locations.data)) {
      this.props.loadLocations()
    }
  }

  render () {
    const { locations } = this.props
    const locOptions = locationsToOptions(locations)
    const selectedLocation = locations.data[this.state.locationId]

    const { startDateStr, startTimeStr } = this.state
    const timeOptions = startDateStr
                      ? generateTimeStrs(startDateStr).map(x => ({ value: x, label: x }))
                      : []
    const selectedTime = startTimeStr ? { value: startTimeStr, label: startTimeStr } : null


    // Restrict user input according to states
    const shouldShowStartDateInput = this.state.locationId && true
    const shouldShowStartTimeInput = shouldShowStartDateInput && this.state.startDateStr && true
    const shouldShowDurationInput = shouldShowStartTimeInput && this.state.startTimeStr && true
    const isInputValid = this.state.locationId &&
                         this.state.startDateStr &&
                         this.state.startTimeStr &&
                         this.state.duration

    return (
      <Fragment>
        {locations.pending
         ? (
           <div className="center">
             <BouncingLoader />
           </div>
         )
         : (
           <form onSubmit={this.handleSubmit}>
             <section>
               <div className="row row--form">
                 <div className="twelve cols">
                   <span className="label">
                     Where do you want to park? <button className="link">Show me the map</button>
                   </span>
                   <Select
                     options={locOptions}
                     selectedOption={selectedLocation && { value: selectedLocation.id,
                                                           label: selectedLocation.name }}
                     onSelect={this.handleSelectLocation}
                   />
                 </div>
               </div>
             </section>

             <section>
               <div className="row row--form">
                 <div className="eight cols">
                   <Mask enabled={!shouldShowStartDateInput}>
                     <label htmlFor="start-date">On which day?</label>
                     <input name="startDate" id="start-date" type="date"
                            min={this.todayStr}
                            value={this.state.startDateStr}
                            onChange={this.handleSelectStartDate}/>
                   </Mask>
                 </div>
                 <div className="four cols">
                   <Mask enabled={!shouldShowStartTimeInput}>
                     <span className="label">
                       Start from when?
                     </span>
                     <Select
                       options={timeOptions}
                       selectedOption={selectedTime}
                       onSelect={this.handleSelectStartTime}
                     />
                   </Mask>
                 </div>
               </div>
             </section>

             <section>
               <div className="row row--form">
                 <div className="twelve cols">
                   <Mask enabled={!shouldShowDurationInput}>
                     <label htmlFor="duration">
                       Duration: <span className="output__output">{this.state.duration}</span> mintues.
                       Estimated Cost: <span className="output__unit">$</span><span className="output__output">{this.estimateCost().toFixed(2)}</span>
                     </label>
                     <Range
                       value={this.state.duration}
                       step={15}
                       onChange={x => {this.setState({ duration: x })}}
                       range={[0, this.state.maxDuration]}
                     />
                   </Mask>
                 </div>
               </div>
             </section>

             <div className="divider"></div>

             <section>
               <div className="row row--form">
                 <Mask enabled={!isInputValid}>
                   <button className="button--primary">Confirm</button>
                 </Mask>
               </div>
             </section>
           </form>
         )}
        <style jsx>{`
.link {
  opacity: 0.7;
  text-decoration: underline;
  display: inline-block;
}
          `}
        </style>
      </Fragment>
    )
  }

  handleSelectLocation = (x) => {
    this.setState({
      locationId: x.value,
      startDateStr: '',
      startTimeStr: '',
      maxDuration: 0
    })
  }

  handleSelectStartDate = (e) => {
    this.setState({
      startDateStr: e.target.value,
      startTimeStr: '',
      duration: 0,
      maxDuration: 0
    })
  }

  handleSelectStartTime = async (x) => {
    const startTimeStr = x.value
    const { startDateStr, locationId } = this.state
    const startTime = parseTimeStr(`${startDateStr}T${startTimeStr}`)

    this.setState({ maxDuration: 0 })

    const res = await request.get(`${urls.api}/locations/${locationId}/duration`,
                                  true,
                                  { 'start': startTime })
    switch (res.type) {
      case 'SUCCESS':
        const { duration, alternative } = res.data
        if (alternative) {
          const newStart = new Date(alternative)
          const newDate = formatTime(newStart, 'YYYY-MM-DD')
          const newTime = formatTime(newStart, 'HH:mm')
          if (window.confirm(`We are sorry but no spots are available at that time. Do you want to change to ${newStart.toLocaleString()}?`)) {
            this.setState({
              startDateStr: newDate,
              startTimeStr: newTime,
              maxDuration: duration
            })
          } else {
            this.setState({
              startTimeStr: '',
              maxDuration: 0,
            })
          }
        } else {
          this.setState({
            startTimeStr,
            maxDuration: duration
          })
        }
        break

      case 'ERROR':
        console.log('Error')
        break
    }
  }

  handleSubmit = async (e) => {
    e.preventDefault()

    const { startTimeStr, startDateStr, locationId, duration } = this.state
    const startTime = parseTimeStr(`${startDateStr}T${startTimeStr}`)
    const endTime = new Date(startTime.valueOf())
    endTime.setMinutes(endTime.getMinutes() + duration)
    const res = await request.post(
      `${urls.api}/me/reservations`,
      true,
      null,
      {
        'location_id': locationId,
        start: startTime.toISOString(),
        end: endTime.toISOString()
      })

    switch (res.type) {
      case 'SUCCESS':
      case 'ERROR':
        this.props.onSuccess()
        break;
    }
  }

  estimateCost = (): number => {
    const { duration } = this.state
    return duration * 0.15
  }
}
