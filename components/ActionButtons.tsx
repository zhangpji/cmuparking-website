import * as React from 'react'
import { Fragment } from 'react'
import css from 'styled-jsx/css'
import { IoPlus } from 'react-icons/lib/io'

interface ActionButtonProps {
  onClick: () => void
}

export const NewReservationButton: React.SFC<ActionButtonProps> = (props) => (
  <Fragment>
    <button onClick={props.onClick} className="button--primary">
      <span className="icon">
        <IoPlus />
      </span>
      <span>New Reservation</span>
    </button>
    <style jsx>{styles}</style>
  </Fragment>
)

export const NewViolationButton: React.SFC<ActionButtonProps> = (props) => (
  <Fragment>
    <button onClick={props.onClick} className="link">
      <span>Report Violation →</span>
    </button>
    <style jsx>{styles}</style>
  </Fragment>
)

const styles = css`
button {
  display: flex;
  align-items: center;
}

.button--primary {
  padding: 6px 10px;
}

.icon {
  color: #efefea;
}

button {
  vertical-align: middle;
}

button span + span {
  margin-left: 0.3em;
}`
