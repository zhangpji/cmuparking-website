import * as React from 'react'
import { Fragment } from 'react'
import BaseIcon from 'react-icon-base'
import { IoHeart, IoFlash, IoWand  } from 'react-icons/lib/io'

const features = [
  {
    title: 'Convenient',
    description: 'Reservation, payment, violation reporting. Everything on parking can be done within a single system.',
    icon: IoHeart
  },
  {
    title: 'Efficient',
    description: 'Crowd-sourcing violation monitoring, pay-as-you-park mechanism and dynamic pricing will maximize the system efficiency.',
    icon: IoFlash
  },
  {
    title: 'Smart',
    description: 'Dynamic pricing based on data analytics and smart spot allocation ensure the resources will never be wasted in theory.',
    icon: IoWand
  }
]

interface CardProps {
  title: string
  description: string
  icon: typeof BaseIcon
}

const Card: React.SFC<CardProps> = (props) => (
  <Fragment>
    <div className="card">
      <div className="icon">
        <props.icon />
      </div>
      <h3>{props.title}</h3>
      <p>{props.description}</p>
    </div>
    <style jsx>{`
.card {
  padding: 1em;
  text-align: center;
  flex-basis: 33.33%;
}

.icon {
  text-align: center;
  margin-bottom: 0.7em;
  font-size: 2em;
}

h3 {
  font-size: 1rem;
  margin: 0em auto 1em;
}
      `}
    </style>
  </Fragment>
)

export const Feature: React.SFC<{}> = () => (
  <Fragment>
    <section className="feature">
      <div className="inner">
        <div className="cards">
          {features.map((feature) => (
            <Card key={feature.title}
                  title={feature.title}
                  description={feature.description}
                  icon={feature.icon} />))
          }
        </div>
      </div>
    </section>
    <style jsx>{`
.inner {
  padding: 3em 0;
}

.title {
  font-size: 2rem;
  text-align: center;
  color: rgba(0, 0, 0, 0.07);
}

.cards {
  display: flex;
  justify-content: center;
  align-items: start;
}

@media(max-width: 720px) {
  .cards {
    display: block;
  }
}
      `}
    </style>
  </Fragment>
)
