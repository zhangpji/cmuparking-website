import * as React from 'react'
import { Fragment } from 'react'
import { Message, MessageType } from '../store/states'

interface BannerProps {
  messageId: string
  message: Message
  delMessage: (string) => void
}

const messageTypeToColor = new Map<MessageType, string>(
  [
    ['ERROR', '#c51350'],
    ['SUCCESS', '#169954']
  ]
)

export const Banner: React.SFC<BannerProps> = (props) => (
  <Fragment>
    <div className="banner">
      <div className="inner bar">
        <span className="message">{props.message.message}</span>
        <span className="close"
              onClick={() => {props.delMessage(props.messageId)}}>×</span>
      </div>
    </div>
    <style jsx>{`
.banner {
  color: #fff;
  margin-top: 3px;
}

.inner {
  padding: 0.7em 0;
}

.message {
  margin: 0 1em;
}

.close {
  margin: 0 1em;
  cursor: pointer;
}
      `}
    </style>
    <style jsx>{`
.inner {
  background-color: ${messageTypeToColor.get(props.message.type)};
  border-radius: 3px;
}
      `}
    </style>
  </Fragment>
)
