import * as React from 'react'
import { Fragment } from 'react'
import { IoLink } from 'react-icons/lib/io'

export const Subscription: React.SFC<{}> = () => (
  <Fragment>
    <section className="subscription">
      <div className="inner">
        <form action="https://tinyletter.com/cmumac" method="post"
              className="form--small">
          <h2 className="title"><label htmlFor="email">Get Updates</label></h2>
          <input name="embed" type="hidden" value="1" />
          <div className="form__row">
            <input className="full"
                   id="email" type="email" placeholder="Your Email Address" />
          </div>
          <button type="submit" className="link center">
            <span className="icon">
              <IoLink />
            </span>
            Subscribe
          </button>
          <p><strong>No spam, ever.</strong> We will never share your email address and you can opt out at any time.</p>
        </form>
      </div>
    </section>
    <style jsx>{`
.subscription {
  background-color: rgba(0, 0, 0, 0.07);
}

.inner {
  padding: 3em 0 2em;
}

.title {
  font-size: 2rem;
  text-align: center;
  color: rgba(0, 0, 0, 0.5);
  margin-bottom: 1.5em;
}

.form--small {
  margin: 0 auto;
}

input {
  text-align: center;
  padding: 12px 20px;
  border: 0;
}

button {
  text-transform: uppercase;
  letter-spacing: 0.2em;
  padding: 0 4px;
  margin: 1em auto;
}

p {
  text-align: center;
  color: rgba(0, 0, 0, 0.5);
}
      `}
    </style>
  </Fragment>
)
