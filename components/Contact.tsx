import * as React from 'react'
import { Fragment } from 'react'

export const Contact: React.SFC<{}> = () => (
  <Fragment>
    <section className="contact">
      <form action="https://formspree.io/cmumactrack@gmail.com" method="post">
        <input type="hidden" name="_format" value="plain" />
        <div className="row row--form">
          <div className="six cols">
            <label htmlFor="email">Email</label>
            <input type="email" name="email" id="email"
                   placeholder="Your email" required />
          </div>
          <div className="six cols">
            <label htmlFor="name">Name</label>
            <input name="name" type="text" id="name"
                   placeholder="Your name (optional)" />
          </div>
        </div>
        <div className="row row--form">
          <div className="twelve cols">
            <label htmlFor="subject">Subject</label>
            <input id="subject" name="_subject"
                   type="text" placeholder="Subject (optional)" />
          </div>
        </div>
        <div className="row row--form">
          <div className="twelve cols">
            <label htmlFor="message">Content</label>
            <textarea id="message"
                      name="message"
                      placeholder="What do you want to tell us?"
                      required>
            </textarea>
          </div>
        </div>
        <div className="row row--form">
          <button type="submit" className="button--primary">Send</button>
        </div>
      </form>
    </section>
  </Fragment>
)
