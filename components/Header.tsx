import * as React from 'react'
import { Fragment } from 'react'
import Link from 'next/link'

export const Header: React.SFC<{}> = () => (
  <Fragment>
    <header>
      <div className="inner center">
        <div className="logo">
          <img alt="CMU Parking LOGO" src="/static/logo.png" />
        </div>
        <div className="intro">
          <h1 className="title">CMU Parking</h1>
          <p className="description">Reserve, pay and park. Parking, made easy.</p>
          <div className="links">
            <Link prefetch href="/signup"><a className="button button--primary">Sign up</a></Link>
            <Link prefetch href="/"><a>Download</a></Link>
          </div>
        </div>
      </div>
    </header>
    <style jsx>{`
.inner {
  padding: 3em 4em;
}

.button--primary {
  font-weight: 700;
}

.logo {
  width: 300px;
  padding: 50px 10px;
  text-align: center;
}

.title {
  margin-bottom: 0.2rem;
}

.description {
  margin: 0;
  font-size: 1.5em;
  font-weight: normal;
  color: rgba(0, 0, 0, 0.5);
}

.links {
  margin-top: 2em;
}

.links a:not(:last-child) {
  margin-right: 1em;
}

@media(max-width: 720px) {
  .inner {
    padding: 2em 1em 1em;
  }

  .logo {
    display: none;
  }
}

      `}
    </style>
  </Fragment>
)
