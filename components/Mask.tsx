import * as React from 'react'
import { Fragment } from 'react'

interface MaskProps {
  enabled?: boolean
}

export const Mask: React.SFC<MaskProps> = (props) => (
  <Fragment>
    <div className={`mask${props.enabled ? ' disabled' : ''}`}>
      {props.children}
    </div>
  </Fragment>
)
