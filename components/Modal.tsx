import * as React from 'react'
import { Fragment } from 'react'
import { IoClose } from 'react-icons/lib/io'

interface ModalProps {
  title: string
  onClose: () => void
}

export const Modal: React.SFC<ModalProps> = (props) => (
  <Fragment>
    <div className="modal" onClick={props.onClose}>
      <div className="modal__body" onClick={(e) => {e.stopPropagation()}}>
        <div className="modal__title">
          <h2>{props.title}</h2>
          <span className="icon close" onClick={props.onClose}>
            <IoClose />
          </span>
        </div>
        <div className="modal__content">{props.children}</div>
      </div>
    </div>
    <style jsx>{`
.close {
  cursor: pointer;
  opacity: 0.5;
}

.modal__title {
  display: flex;
  align-items: flex-start;
}
      `}
    </style>
  </Fragment>
)
