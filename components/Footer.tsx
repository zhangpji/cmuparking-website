import * as React from 'react'
import { Fragment } from 'react'
import Link from 'next/link'

export const Footer: React.SFC<{}> = () => (
  <Fragment>
    <footer>
      <div className="inner bar">
        <div className="links">
          <Link prefetch href="/about"><a>About</a></Link>
          <Link prefetch href="/faq"><a>FAQ</a></Link>
          <Link prefetch href="/contact"><a>Contact</a></Link>
          <Link prefetch href="/terms"><a>Terms</a></Link>
        </div>
        <div className="copy">
          © 2018, <a href="http://mac.heinz.cmu.edu/">Mobility Data Analytics Center</a>.
        </div>
      </div>
    </footer>
    <style jsx>{`
.links a {
  margin-right: 1em;
}

.bar {
  flex-wrap: wrap;
}

@media(max-width: 720px) {
  .bar {
    justify-content: center;
  }
}
      `}
    </style>
  </Fragment>
)
