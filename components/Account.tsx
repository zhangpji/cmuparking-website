import * as React from 'react'
import { Fragment } from 'react'
import Link from 'next/link'
import { BouncingLoader } from './Loader'
import { fetchTokens, saveTokens, signUp } from '../utils/auth'

const normalizePhoneNumber = (phoneNumber: string): string => (
  phoneNumber.replace(/-/g, '')
)

interface AccountProps {
  onSuccess: () => void
  addInfo: (msg: string) => void
  addError: (msg: string) => void
}

type AccountState = {
  pending: boolean
  email: string
  password: string
  permanent: boolean
}

export class Account extends React.Component<AccountProps, AccountState> {
  constructor (props) {
    super(props)
    this.state = {
      pending: false,
      email: '',
      password: '',
      permanent: false,
    }
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value })
  }

  handleCheck = () => {
    this.setState(prevState => (
      { permanent: !prevState.permanent }
    ))
  }

  handleSubmit = async (e) => {
    e.preventDefault()
    this.setState({ pending: true })

    const { email, password, permanent } = this.state

    const storage = permanent ? localStorage : sessionStorage
    const tokens = await fetchTokens(email, password)

    switch (tokens.type) {
      case 'SUCCESS':
        saveTokens(tokens.data, storage)
        this.props.onSuccess()
        break

      case 'ERROR':
        for (let msg of tokens.messages) {
          this.props.addError(msg)
        }
    }

    this.setState({ pending: false })
  }

  render () {
    const { pending } = this.state
    return (
      <Fragment>
        {pending
         ? <BouncingLoader />
         : (
           <form onSubmit={this.handleSubmit} className="form--small">
             <h1 className="form__title">Log into <Link href="/"><a>CMU Parking</a></Link></h1>
             <div className="row row--form">
               <div className="twelve cols">
                 <label htmlFor="email">Email</label>
                 <input name="email" id="email" type="email" required
                        placeholder="example@cmu.edu"
                        value={this.state.email} onChange={this.handleChange} />
               </div>
             </div>

             <div className="row row--form">
               <div className="twelve cols">
                 <label htmlFor="password">Password</label>
                 <input name="password" id="password" type="password" required
                        placeholder="PASSWORD"
                        value={this.state.password} onChange={this.handleChange} />
               </div>
             </div>

             <div className="row row--form">
               <div className="twelve cols form__checkbox">
                 <input name="permanent" type="checkbox" id="permanent"
                        checked={this.state.permanent} onChange={this.handleCheck} />
                 <label htmlFor="permanent" className="three-fourths">Remember me?</label>
               </div>
             </div>

             <div className="row row--form">
               <button type="submit" className="button--primary">Sign in</button>
             </div>

             <div>
               New to CMU Parking? <Link href="/signup"><a>Sign up</a></Link> now.
             </div>
           </form>
         )}
      </Fragment>
    )
  }
}

interface NewAccountProps {
  onSuccess: () => void
  addError: (msg: string) => void
  addInfo: (msg: string) => void
}

type NewAccountState = {
  pending: boolean
  email: string
  password: string
  name: string
  plateNumber: string
  phoneNumber: string
}

export class NewAccount extends React.Component<NewAccountProps, NewAccountState> {
  constructor (props) {
    super(props)
    this.state = {
      pending: false,
      email: '',
      password: '',
      name: '',
      plateNumber: '',
      phoneNumber: ''
    }
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value })
  }

  handleSubmit = async (e) => {
    e.preventDefault()

    this.setState({ pending: true })
    const { email, name, password, plateNumber, phoneNumber } = this.state

    const res = await signUp(email, name, password, normalizePhoneNumber(phoneNumber), plateNumber)

    switch (res.type) {
      case 'SUCCESS':
        this.props.onSuccess()
        break

      case 'ERROR':
        for (let msg of res.messages) {
          this.props.addError(msg)
        }
    }
    this.setState({ pending: false })
  }

  render () {
    const { pending } = this.state
    return (
      <Fragment>
        <form onSubmit={this.handleSubmit} className="form--small">
          <h1 className="form__title">Join <Link href="/"><a>CMU Parking</a></Link></h1>
          <div className="row row--form">
            <div className="twelve cols">
              <label htmlFor="email">Email</label>
              <input name="email" id="email" type="email" required disabled={pending}
                     placeholder="exmaple@cmu.edu"
                     value={this.state.email} onChange={this.handleChange} />
            </div>
          </div>

          <div className="row row--form">
            <div className="twelve cols">
              <label htmlFor="password">Password</label>
              <input name="password" id="password" type="password" required
                     disabled={pending}
                     placeholder="At least 8 characters"
                     value={this.state.password} onChange={this.handleChange} />
            </div>
          </div>

          <div className="row row--form">
            <div className="twelve cols">
              <label htmlFor="name">Name</label>
              <input name="name" id="name" type="text" disabled={pending}
                     placeholder="John Doe"
                     value={this.state.name} onChange={this.handleChange} />
            </div>
          </div>

          <div className="row row--form">
            <div className="twelve cols">
              <label htmlFor="phone-number">
                Phone Number (For notification only. Optional.)
              </label>
              <input name="phoneNumber" type="tel"
                     value={this.state.phoneNumber}
                     placeholder="412-555-5555"
                     onChange={this.handleChange} />
            </div>
          </div>

          <div className="row row--form">
            <div className="twelve cols">
              <label htmlFor="plate-number">Plate Number</label>
              <input name="plateNumber" id="plate-number" type="text" disabled={pending}
                     placeholder="ABC-1234"
                     value={this.state.plateNumber} onChange={this.handleChange} />
            </div>
          </div>

          {pending
           ? <BouncingLoader />
           : (
             <div className="row row--form">
               <button type="submit" disabled={pending} className="button--primary">
                 Sign up
               </button>
             </div>
             )}

            <div>
              Having an account? <Link href="/signin"><a>Sign in</a></Link> now.
            </div>
          </form>
        </Fragment>
      )
    }
  }
