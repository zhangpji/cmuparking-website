// urls
const backendURL = process.env.BACKEND_URL

export const urls = {
  base: backendURL,
  api: `${backendURL}/api`,
  auth: `${backendURL}/auth`,
}
