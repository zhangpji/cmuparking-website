const withTypescript = require('@zeit/next-typescript')
const withCSS = require('@zeit/next-css')
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin')

module.exports = withTypescript(withCSS(({
  webpack (config, { isServer, dev }) {
    config.plugins = config.plugins.filter((p) => (
      p.constructor.name !== 'FriendlyErrorsWebpackPlugin'
    ))
    if (dev && !isServer) {
      config.plugins
        .push(new FriendlyErrorsWebpackPlugin({ clearConsole: false }))
    }
    return config
  },
  distDir: 'build'
})))
