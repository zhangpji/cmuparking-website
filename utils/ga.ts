import ReactGA from 'react-ga'

export const initGA = () => {
  ReactGA.initialize('UA-118288545-1')
}

export const logPageview = () => {
  ReactGA.pageview(`${window.location.pathname}${window.location.search}`)
}
