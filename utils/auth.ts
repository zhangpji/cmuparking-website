import { request, update, Result } from './request'
import { constantly } from './shoyu'
import { urls } from '../constants'

// Storage
let storage: Storage

const accessKey = 'accessToken'
const refreshKey = 'refreshToken'

// Token type
export type Tokens = {
  access: string
  refresh: string
}

// Save and get tokens
export const saveTokens = (tokens: Tokens, location?: Storage): void => {
  storage = location

  const { access, refresh } = tokens
  storage.setItem(accessKey, access)
  storage.setItem(refreshKey, refresh)
}

export const getTokens = (): Tokens => {
  for (let s of [sessionStorage, localStorage]) {
    if (s.getItem(accessKey)) {
      storage = s
      return {
        access: storage.getItem(accessKey),
        refresh: storage.getItem(refreshKey)
      }
    }
  }

  return {
    access: null,
    refresh: null
  }
}

export const hasTokens = (): boolean => {
  const { access, refresh } = getTokens()
  return (access && refresh && true)
}

export const removeTokens = (): void => {
  storage.removeItem(accessKey)
  storage.removeItem(refreshKey)
}

// Fetch tokens from server
export const fetchTokens = async (email: string, password: string): Promise<Result<Tokens>> => {
  const res = await request({
    method: 'post',
    url: `${urls.auth}/new`,
    data: { email, password },
  })

  return update<any, Tokens>(res, (x) => {
    return {
      access: x['access_token'],
      refresh: x['refresh_token'],
    }
  })
}

export const refreshToken = async (): Promise<Result<string>> => {
  const { refresh } = getTokens()
  const res = await request({
    method: 'post',
    url: `${urls.auth}/refresh`,
  }, refresh)

  return update<any, string>(res, (x) => {
    const token = x['access_token']
    storage.setItem(accessKey, token)
    return token
  })
}

export const revokeTokens = async (): Promise<void> => {
  const { access, refresh } = getTokens()
  removeTokens()
  await request({
    method: 'delete',
    url: `${urls.auth}/revoke`,
  }, access)
  await request({
    method: 'delete',
    url: `${urls.auth}/revoke2`,
  }, refresh)
}

export const signUp = async (email: string,
                             name: string,
                             password: string,
                             phoneNumber: string,
                             plateNumber: string): Promise<Result<void>> => {
  const res = await request({
    method: 'post',
    url: `${urls.api}/me`,
    data: {
      email,
      name,
      password,
      'plate_number': plateNumber,
      'phone_number': phoneNumber,
    }
  })

  return update<any, void>(res, constantly(null))
}

// Request with authentication
type requestFnWithAuth<T> = ((
  url: string,
  auth?: boolean,
  params?: { [key: string]: number | string | Date | Array<number | string> },
  data?: any) => Promise<Result<T>>)

const getAuthRequest = <T = any>(method: 'post' | 'get' | 'delete' | 'put' | 'patch'): requestFnWithAuth<T> => {
  return (url, auth, params, data) => {
    if (auth) {
      if (hasTokens()) {
        const { access } = getTokens()
        return request<T>({ url,
                            method,
                            params,
                            data },
                          access,
                          refreshToken)
      } else {
        return new Promise<Result<T>>(resolve => resolve({
          type: 'ERROR',
          messages: []
        }))
      }
    } else {
      return request<T>({ url,
                          method,
                          params,
                          data })
    }
  }
}

export default {
  get: getAuthRequest('get'),
  post: getAuthRequest('post'),
  delete: getAuthRequest('delete'),
  put: getAuthRequest('put'),
  patch: getAuthRequest('patch')
}
