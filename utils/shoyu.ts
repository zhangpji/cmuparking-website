// Async actions
export const delay = (ms: number): Promise<void> => (
  new Promise<void>(resolve => { setTimeout(resolve, ms) })
)

// General helpers
export const constantly = <T>(x: T): ((any) => T) => (_) => x

export const dissoc = <T>(xs: { [key: string]: T }, k: string): { [key: string]: T } => (
  Object.keys(xs)
    .filter(key => key !== k)
    .reduce((ys, key) => ({
      ...ys,
      [key]: xs[key]
    }), {})
)

export const toMap = <T extends { id: string }>(xs: T[]): { [id: string]: T } => (
  xs.reduce((ys, x) => ({
    ...ys,
    [x.id]: x
  }), {})
)

export const isEmptyObj = (x: Object): boolean => {
  if (Object.keys(x).length === 0) {
    return true
  } else {
    return false
  }
}

export const compareField = <T>(x: T, y: T, k: string, onTie?: (x: T, y: T) => number): number => {
  if (x[k] < y[k]) {
    return -1
  } else if (x[k] > y[k]) {
    return 1
  } else {
    if (onTie) {
      return onTie(x, y)
    } else {
      return 0
    }
  }
}


// Application helpers

/// Convert time to a greeting
const timeParts = [
  [6, 'night'],
  [12, 'morning'],
  [18, 'afternoon'],
  [24, 'night'],
]
export const timeToGreeting = (): string => {
  const hr = new Date().getHours()
  const part = timeParts.find(x => (x[0] >= hr))[1]
  return `Good ${part}`
}

/// Date and time
const isSameDay = (x: Date, y: Date): boolean => (
  x.getFullYear() === y.getFullYear() &&
    x.getMonth() === y.getMonth() &&
    x.getDay() === y.getDay()
)

const seqTime = (start: Date, end: Date, interval: number): Date[] => {
  let current = new Date(start.valueOf())
  let result = []
  while (current < end) {
    result.push(new Date(current.valueOf()))
    current.setMinutes(current.getMinutes() + interval)
  }
  return result
}

const endOfDay = (x: Date): Date => (
  new Date(
    x.getFullYear(), x.getMonth(), x.getDate() + 1,
    0, 0, 0, 0
  )
)

const formatStrPattern = (/Y{2,4}|M{1,2}|D{1,2}|H{1,2}|m{1,2}/g)
export const formatTime = (x: Date, formatStr: string): string => {
  return formatStr.replace(
    formatStrPattern,
    (match) => {
      switch (match) {
        case 'YY':
          return `${x.getFullYear()}`.slice(-2)
        case 'YYYY':
          return `${x.getFullYear()}`
        case 'M':
          return `${x.getMonth() + 1}`
        case 'MM':
          return `${x.getMonth() + 1}`.padStart(2, '0')
        case 'D':
          return `${x.getDate()}`
        case 'DD':
          return `${x.getDate()}`.padStart(2, '0')
        case 'H':
          return `${x.getHours()}`
        case 'HH':
          return `${x.getHours()}`.padStart(2, '0')
        case 'm':
          return `${x.getMinutes()}`
        case 'mm':
          return `${x.getMinutes()}`.padStart(2, '0')
      }
    })
}

const timeStrPattern = (/^(\d{4})-(\d{1,2})-(\d{1,2})([T ](\d{1,2}):(\d{1,2}))?.*$/)
export const parseTimeStr = (timeStr: string): Date | null => {
  const parts_ = timeStr.match(timeStrPattern)
  if (parts_) {
    const parts = parts_.map(x => +x)
    return new Date(
      parts[1], parts[2] - 1, parts[3],
      parts[5] || 0, parts[6] || 0
    )
  }
}

export const generateTimeStrs = (date: string | Date): string[] => {
  const start = typeof date === 'string' ? parseTimeStr(date) : date
  const end = endOfDay(start)
  const now = new Date()

  const times = isSameDay(start, now) ?
    seqTime(start, end, 15).filter(x => x > now) :
    seqTime(start, end, 15)

  return times.map(x => formatTime(x, 'HH:mm'))
}
