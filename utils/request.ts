import axios, { AxiosRequestConfig } from 'axios'

// Result type
type Success<T> = {
  type: 'SUCCESS'
  data: T
  messages?: string[]
}

type Error = {
  type: 'ERROR'
  messages: string[]
}

export type Result<T> = Success<T> | Error

export const update = <T, R>(result: Result<T>, fn: (T) => R): Result<R> => {
  switch (result.type) {
    case 'SUCCESS':
      return {
        type: 'SUCCESS',
        data: fn(result.data),
        messages: result.messages,
      }

    case 'ERROR':
      return result
  }
}

// Request functions
const handleError = async <T>(error,
                              fn?: (token: string) => Promise<Result<T>>,
                              refreshToken?: () => Promise<Result<string>>): Promise<Result<T>> => {
  if (error.response && error.response.status === 401 && refreshToken) {
    const newToken = await refreshToken()
    switch (newToken.type) {
      case 'SUCCESS':
        return fn(newToken.data)

      default:
        return {
          type: 'ERROR',
          messages: ['Session expired. Please sign in again.']
        }
    }
  } else {
    return {
      type: 'ERROR',
      messages: [error.message]
    }
  }
}

const paramsSerializer = (params: { [key: string]: number | string | Date | Array<number | string> }): string => (
  Object.keys(params).map<string>(
    key => {
      const val = params[key]
      if (val instanceof Array) {
        return `${key}=${encodeURIComponent(val.join(','))}`
      } else if (val instanceof Date) {
        return `${key}=${val.toISOString()}`
      } else {
        return `${key}=${encodeURIComponent(val.toString())}`
      }
    }
  ).join('&')
)

export const request = async <T>(config: AxiosRequestConfig,
                                 token?: string,
                                 refreshToken?: () => Promise<Result<string>>): Promise<Result<T>> => {
  try {
    const res = await axios(Object.assign(
      config,
      { paramsSerializer: paramsSerializer },
      token && { headers: Object.assign(
        {}, config.headers,
        { 'Authorization': `Bearer ${token}` })}))

    return {
      type: 'SUCCESS',
      data: res.data
    }
  } catch(error) {
    return await handleError<T>(error,
                                (token) => request(config, token),
                                refreshToken)
  }
}
