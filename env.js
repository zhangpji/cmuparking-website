const prod = process.env.NODE_ENV === 'production'

module.exports = {
  'process.env.BACKEND_URL': prod
    ? 'https://emil2.heinz.cmu.edu'
    : 'http://localhost:5000'
}
