import * as React from 'react'
import { Base,
         Nav,
         Contact as ContactForm,
         Footer } from '../components'
import withRedux from '../store'
import { asyncActions } from '../store/actions'

class Contact extends React.Component<any, any> {
  constructor (props) {
    super(props)
  }

  componentDidMount () {
    if (!this.props.user.name) {
      this.props.loadUser()
    }
  }


  render () {
    return (
      <Base title="Contact - CMU Parking">
        <div className="top">
          <Nav user={this.props.user}
               logout={this.props.logout} />
        </div>
        <main>
          <article className="inner">
            <h1>Contact Us</h1>
            <ContactForm />
          </article>
        </main>
        <Footer />
      </Base>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
})

const mapDispatchToProps = (dispatch) => ({
  logout: (): void => {
    dispatch(asyncActions.logout())
  },
  loadUser: (): void => {
    dispatch(asyncActions.loadUser())
  },
})

export default withRedux(mapStateToProps, mapDispatchToProps)(Contact)
