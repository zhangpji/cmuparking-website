import * as React from 'react'
import Router from 'next/router'
import { Base,
         Nav,
         Banner,
         Account,
         Footer } from '../components'
import withRedux from '../store'
import { actions, asyncActions } from '../store/actions'

class SignIn extends React.Component<any, {}> {
  constructor (props) {
    super(props)

    this.state = {}
  }

  componentDidMount () {
    if (!this.props.user.name) {
      this.props.loadUser()
    } else {
      Router.push('/')
    }
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.user.name) {
      Router.push('/')
    }
  }

  onSuccess = (): void => {
    this.props.loadUser()
  }

  render () {
    return (
      <Base title="Sign In - CMU Parking">
        <div className="top">
          <Nav user={this.props.user}
               logout={this.props.logout} />
          {this.props.messages.map(x => (
            <Banner key={x[0]}
                    messageId={x[0]}
                    message={x[1]}
                    delMessage={this.props.delMessage} />
          ))}
        </div>
        <main className="center">
          <Account onSuccess={this.onSuccess}
                   addInfo={this.props.addInfo}
                   addError={this.props.addError} />
        </main>
        <Footer />
      </Base>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  messages: Object.entries(state.messages),
})

const mapDispatchToProps = (dispatch) => ({
  loadUser: (): void => {
    dispatch(asyncActions.loadUser())
  },
  logout: (): void => {
    dispatch(asyncActions.logout())
  },
  addError: (msg: string): void => {
    dispatch(asyncActions.addError(msg))
  },
  delMessage: (id: string): void => {
    dispatch(actions.delMessage(id))
  },
})

export default withRedux(mapStateToProps, mapDispatchToProps)(SignIn)
