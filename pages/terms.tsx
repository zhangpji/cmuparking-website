import * as React from 'react'
import { Base,
         Nav,
         Footer } from '../components'
import withRedux from '../store'
import { asyncActions } from '../store/actions'

class Terms extends React.Component<any, any> {
  constructor (props) {
    super(props)
  }

  componentDidMount () {
    if (!this.props.user.name) {
      this.props.loadUser()
    }
  }

  render () {
    return (
      <Base title="Terms - CMU Parking">
        <div className="top">
          <Nav user={this.props.user}
               logout={this.props.logout} />
        </div>
        <main>
          <article className="content">
            <div className="inner">
              <h1>Terms</h1>
              <p>TBD</p>
            </div>
          </article>
        </main>
        <Footer />
      </Base>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
})

const mapDispatchToProps = (dispatch) => ({
  logout: (): void => {
    dispatch(asyncActions.logout())
  },
  loadUser: (): void => {
    dispatch(asyncActions.loadUser())
  },
})

export default withRedux(mapStateToProps, mapDispatchToProps)(Terms)
