import * as React from 'react'
import Router from 'next/router'
import { Base,
         Nav,
         Banner,
         NewAccount,
         Footer } from '../components'
import withRedux from '../store'
import { actions, asyncActions } from '../store/actions'

class SignUp extends React.Component<any, {}> {
  constructor (props) {
    super(props)
    this.state = {}
  }

  componentDidMount () {
    if (!this.props.user.name) {
      this.props.loadUser()
    } else {
      Router.push('/')
    }
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.user.name) {
      Router.push('/')
    }
  }

  onSuccess = (): void => {
    this.props.clearMessages()
    this.props.addSuccess('Successfully created account. Please sign in to continue.')
    Router.push('/signin')
  }

  render () {
    return (
      <Base title="Sign Up - CMU Parking">
        <div className="top">
          <Nav user={this.props.user}
               logout={this.props.logout} />
          {this.props.messages.map(x => (
            <Banner key={x[0]}
                    messageId={x[0]}
                    message={x[1]}
                    delMessage={this.props.delMessage} />
          ))}
        </div>
        <main className="center">
          <NewAccount onSuccess={this.onSuccess}
                      addError={this.props.addError}
                      addInfo={this.props.addInfo} />
        </main>
        <Footer />
      </Base>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  pending: state.signingUp,
  messages: Object.entries(state.messages),
})

const mapDispatchToProps = (dispatch) => ({
  logout: (): void => {
    dispatch(asyncActions.logout())
  },
  loadUser: (): void => {
    dispatch(asyncActions.loadUser())
  },
  addSuccess: (msg: string): void => {
    dispatch(asyncActions.addSuccess(msg))
  },
  addError: (msg: string): void => {
    dispatch(asyncActions.addError(msg))
  },
  delMessage: (id: string): void => {
    dispatch(actions.delMessage(id))
  },
  clearMessages: (): void => {
    dispatch(actions.clearMessages())
  }
})

export default withRedux(mapStateToProps, mapDispatchToProps)(SignUp)
