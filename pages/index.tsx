import * as React from 'react'
import { Fragment } from 'react'
import { Base,
         Nav,
         Banner,
         Header,
         Feature,
         Subscription,
         ActionBar,
         NewReservationButton,
         NewViolationButton,
         ReservationList,
         Modal,
         NewReservation,
         NewViolation,
         Footer } from '../components'
import withRedux from '../store'
import { actions, asyncActions } from '../store/actions'

class Index extends React.Component<any, any> {
  constructor (props) {
    super(props)
    this.state = {
      isAddingNew: false,
      isReportingViolation: false,
    }
  }

  componentDidMount () {
    if (!this.props.user.name) {
      this.props.loadUser()
    }
  }

  renderMain () {
    return (
      <section className="inner content">
        <ActionBar title="My Reservations">
          <NewReservationButton onClick={() => {this.setState({ isAddingNew: true })}} />
          <NewViolationButton onClick={() => {this.setState({ isReportingViolation: true })}} />
        </ActionBar>
        <ReservationList
          reservations={this.props.reservations}
          locations={this.props.locations}
          loadReservations={this.props.loadReservations}
          loadLocations={this.props.loadLocations}
        />
        {this.state.isAddingNew
         && (
           <Modal title="New Reservation"
                  onClose={() => {this.setState({ isAddingNew: false })}}>
             {this.props.messages.map(x => (
               <Banner key={x[0]}
                       messageId={x[0]}
                       message={x[1]}
                       delMessage={this.props.delMessage} />
             ))}
             <NewReservation
               locations={this.props.locations}
               loadLocations={this.props.loadLocations}
               onSuccess={() => {
                   this.setState({ isAddingNew: false })
                   this.props.addSuccess('Successfully created a new reservation.')
                   this.props.loadReservations()
               }}
             />
           </Modal>
         )}
        {this.state.isReportingViolation
         && (
           <Modal title="Report Violation"
                  onClose={() => {this.setState({ isReportingViolation: false })}}>
             {this.props.messages.map(x => (
               <Banner key={x[0]}
                       messageId={x[0]}
                       message={x[1]}
                       delMessage={this.props.delMessage} />
             ))}
             <NewViolation
               locations={this.props.locations}
               loadLocations={this.props.loadLocations}
               onSuccess={() => {
                   this.setState({ isReportingViolation: false })
                   this.props.addSuccess('Successfully reported a new violation.')
               }}
             />
           </Modal>
         )}
      </section>
    )
  }

  render () {
    const hasPopup = this.state.isAddingNew || this.state.isReportingViolation
    return (
      <Base title="CMU Parking">
        <div className="top">
          <Nav user={this.props.user}
               logout={this.props.logout} />
          {!hasPopup &&
           this.props.messages.map(x => (
             <Banner key={x[0]}
                     messageId={x[0]}
                     message={x[1]}
                     delMessage={this.props.delMessage} />
           ))}
        </div>
        <main>
          {this.props.user.email
           ? (this.renderMain())
           : (
             <Fragment>
               <Header />
               <Feature />
               <Subscription />
             </Fragment>
           )}
        </main>
        <Footer />
      </Base>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  locations: state.locations,
  reservations: state.reservations,
  messages: Object.entries(state.messages),
})

const mapDispatchToProps = (dispatch) => ({
  logout: (): void => {
    dispatch(asyncActions.logout())
  },
  loadLocations: (): void => {
    dispatch(asyncActions.loadLocations())
  },
  loadReservations: (): void => {
    dispatch(asyncActions.loadReservations())
  },
  loadUser: (): void => {
    dispatch(asyncActions.loadUser())
  },
  addSuccess: (msg: string): void => {
    dispatch(asyncActions.addSuccess(msg))
  },
  addError: (msg: string): void => {
    dispatch(asyncActions.addError(msg))
  },
  delMessage: (id: string): void => {
    dispatch(actions.delMessage(id))
  },
})

export default withRedux(mapStateToProps, mapDispatchToProps)(Index)
